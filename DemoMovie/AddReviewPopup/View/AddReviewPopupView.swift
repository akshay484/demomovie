//
//  AddReviewPopupView.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit
import Cosmos

class AddReviewPopupView: AppMainVc {
    
    @IBOutlet weak var btnAddReview: UIButton!
    @IBOutlet weak var viewCosmos: CosmosView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    weak var addReviewViewModelDelegate: AddReviewViewModelDelegate!
    lazy var addReviewViewModel: AddReviewViewModel = AddReviewViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUISettings()
        // Do any additional setup after loading the view.
    }
    

    
    //MARK: - set UI settings
    func setUISettings(){
        self.btnAddReview.setButtonStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 14, text: "DONE",backgroundColor: ColorCodes.greenColor)
        self.viewBackground.layer.cornerRadius = 8
        self.btnAddReview.layer.cornerRadius = 8
        self.lblTitle.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.blackColor, fontSize: 14, text: "Add Rating")
    }
    
    @IBAction func addRating(_ sender: UIButton){
        let dictParam: NSDictionary = [MovieRatingKeys.api_key: API.APIKey,
                         MovieRatingKeys.session_id: ""]
        self.addReviewViewModel.postAddReviewToServer(methodName: API.addRating, delegate: self, bodyParam: dictParam)
    }
    
}



//MARK: - API methods for adding ratings
extension AddReviewPopupView: AddReviewViewModelDelegate{
    
    
    
    
    func didFetchAddReviewData(responseData: Any) {
        self.addReviewViewModel.parseSuccessData(completion: { isSuccess, message, title, resultObj in
            if(isSuccess){
                
                    let transition = CATransition()
                    transition.duration = 0.5
                    transition.type = CATransitionType.fade
                    transition.subtype = CATransitionSubtype.fromTop
                    transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                    self.view.window!.layer.add(transition, forKey: kCATransition)
                    self.dismiss(animated: true, completion: nil)
                
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    
    func didFailAddReviewData(error: NSError) {
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
  
    
}
