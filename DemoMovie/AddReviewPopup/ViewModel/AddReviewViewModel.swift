//
//  AddReviewViewModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  AddReview delegate to get success/failure from API

protocol AddReviewViewModelDelegate: AnyObject {
    func didFetchAddReviewData(responseData: Any)
    func didFailAddReviewData(error: NSError)
}

class AddReviewViewModel:Networking,MainServiceDelegate{
    
    
    weak var addReviewViewModelDelegate: AddReviewViewModelDelegate?
    var currentAPI = ""
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
        if(currentAPI == API.addRating){
            self.addReviewViewModelDelegate?.didFetchAddReviewData(responseData: responseData)
        }
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
        if(currentAPI == API.addRating){
            self.addReviewViewModelDelegate?.didFailAddReviewData(error: error)
        }
    }
    
    
    //MARK:-  Service call for add review
    
    func postAddReviewToServer(methodName: String , delegate:AddReviewViewModelDelegate,bodyParam: NSDictionary) -> Void {
        self.currentAPI = methodName
        self.addReviewViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.baseURL + methodName
        super.postCallWithAlamofire(serverUrl: url, bodyParameter: bodyParam)
    }
    
    
    //MARK: - parse success post data
    func parseSuccessData(completion: @escaping ((Bool , String , String , GenresModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<GenresModel>().map(JSONObject: responseDict)
            completion(true , "" , "Alert",jsonData)
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
}

