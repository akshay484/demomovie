//
//  GenreCollectionViewCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import UIKit

class GenreCollectionViewCell: UICollectionViewCell {
    
    //MARK: - set outlets for the ui elements
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var lblGenreTitle: UILabel!
    
    
    override func awakeFromNib() {
        //MARK: - set random color to the cell background
        self.viewParent.backgroundColor = UIColor.init(hexString: ColorCodes.whiteColor)
    }
}
