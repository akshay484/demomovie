//
//  GenresModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 02/09/21.
//

import Foundation
import ObjectMapper

class GenresModel : Mappable {
    var genres : [GenresModelData]?
    var status_code: Int?
    var status_message: String?
    var success: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        genres <- map["genres"]
        status_code <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
    }
    
}

class GenresModelData : Mappable {
    var id : Int?
    var name : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
    
}

