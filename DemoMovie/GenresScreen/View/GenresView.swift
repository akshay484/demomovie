//
//  GenresView.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import UIKit

class GenresView: AppMainVc {
    
    //MARK: - set outlets for the ui elements
    @IBOutlet weak var colGenreList: UICollectionView!
    @IBOutlet weak var imgMoodBackground: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblNavBarHeading: UILabel!
    private let spacing:CGFloat = 16.0
    weak var setSelectedGenreDelegate: SetSelectedGenreDelegate?
    
    private lazy var genreViewModel: GenresViewModel = GenresViewModel()
    private var genresModel: GenresModel?{
        didSet{
            DispatchQueue.main.async {
                self.colGenreList.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - set genre list collectionview delegate and data source methods
        self.colGenreList.delegate = self
        self.colGenreList.dataSource = self
        
        self.setUISettings()
        //MARK:- check if the internet connection is available before making an API call
        NetworkManager.isReachable { networkManagerInstance  in
            if(networkManagerInstance.reachability.isReachable){
            self.getGenresListData()
            }else{
                super.showAleartViewWithTitle(ErrorMessages.noInternetTitle, message: ErrorMessages.noInternetMessage)
            }
        }
    }
    
    
    //MARK: - set UI settings
    func setUISettings(){
        self.viewBackground.backgroundColor = UIColor.init(hexString: ColorCodes.cardShadowColor)
        self.lblNavBarHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: "Genres")
    }
    
    
    //MARK: - dismiss self on click of back button
    @IBAction func dismissSelf(_ sender: UIButton){
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - CollectionView delegate and data source methods
extension GenresView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    //MARK: - collectionView numberOfItemsInSection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.genresModel?.genres?.count ?? 0
    }
    
    //MARK: - collectionView cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.genreCollectionViewCell, for: indexPath) as? GenreCollectionViewCell{
            
            if let genreObj = self.genresModel?.genres?[indexPath.row]{
                //MARK: - set corner radius to the parent view
                cell.viewParent.layer.cornerRadius = 8
                
                if AppInstance.sharedInstance.selectedGenreID == genreObj.id{
                    cell.viewParent.layer.borderWidth = 1
                    cell.viewParent.layer.borderColor = UIColor.init(hexString: ColorCodes.orangeColor).cgColor
                    //MARK: - set UI settings to the labels
                    cell.lblGenreTitle.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.orangeColor, fontSize: 14, text: genreObj.name ?? "")
                }else{
                    cell.viewParent.layer.borderWidth = 0
                    cell.viewParent.layer.borderColor = UIColor.init(hexString: ColorCodes.whiteColor).cgColor
                    //MARK: - set UI settings to the labels
                    cell.lblGenreTitle.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 14, text: genreObj.name ?? "")
                }
            }
            
            
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    
    //MARK: - collectionView collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (1 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width/2)
    }
    
    
    //MARK: - on click of any item from the collection view we are redirecting the user to the home screen
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let genreObj = self.genresModel?.genres?[indexPath.row]{
            self.setSelectedGenreDelegate?.setSelectedGenre(selItem: genreObj)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // fetch the animation from the TableAnimation enum and initialze the TableViewAnimator class
        let animation = CollectionAnimationFactory.makeMoveUpAnimation(rowHeight: cell.frame.height, duration: 0.5,delayFactor: 0.1)
        let animator = CollectionViewAnimator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: collectionView)
    }
}


//MARK: - API Methods and data parsing methods
extension GenresView:GenresViewModelDelegate {
    
    
    //MARK: - Make API Call for getting genres list data
    func getGenresListData(){
        self.genreViewModel.getGenresDataFromServer(methodName: API.getGenreList, delegate: self, bodyParam: "?language=en-US&api_key=\(API.APIKey)")
    }
    
    //MARK: - success method for genre api if it succeeds
    func didFetchGenreData(responseData: Any) {
        self.genreViewModel.parseGenreListData(completion: { isSuccess, message, title, dataObj in
            if(isSuccess){
                self.genresModel = dataObj
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    //MARK: - failure method for genre api if it fails
    func didFailFetchGenreData(error: NSError) {
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
    
}
