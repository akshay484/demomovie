//
//  GenresViewModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 02/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  GenresViewModel delegate to get success/failure from API

protocol GenresViewModelDelegate: AnyObject {
    func didFetchGenreData(responseData: Any)
    func didFailFetchGenreData(error: NSError)
}

class GenresViewModel:Networking,MainServiceDelegate{
    
    
    weak var genresViewModelDelegate: GenresViewModelDelegate?
    var currentAPI = ""
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
        if(currentAPI == API.getGenreList){
            self.genresViewModelDelegate?.didFetchGenreData(responseData: responseData)
        }
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
        if(currentAPI == API.getGenreList){
            self.genresViewModelDelegate?.didFailFetchGenreData(error: error)
        }
    }
    
    
    //MARK:-  Service call for genre data
    
    func getGenresDataFromServer(methodName: String , delegate:GenresViewModelDelegate,bodyParam: String) -> Void {
        self.currentAPI = methodName
        self.genresViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.baseURL + methodName + bodyParam
        super.getCallWithAlamofire(serverUrl: url)
    }
    
    
    //MARK: - parse genre list data
    func parseGenreListData(completion: @escaping ((Bool , String , String , GenresModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<GenresModel>().map(JSONObject: responseDict)
            if(jsonData?.success != 0){
                completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false , jsonData?.status_message ?? "", ErrorMessages.APIAlertTitle,nil)
            }
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
}

