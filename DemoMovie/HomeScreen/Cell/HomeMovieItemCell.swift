//
//  MovieItemCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import UIKit

class HomeMovieItemCell: UITableViewCell {
    
    
    //MARK: - set outlets for the ui elements
    @IBOutlet weak var imgMoviePoster: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblMovieReleaseYear: UILabel!
    @IBOutlet weak var lblMoviePopularity: UILabel!
    @IBOutlet weak var lblMoviePopularityValue: UILabel!
    @IBOutlet weak var lblMovieVoteAvg: UILabel!
    @IBOutlet weak var lblMovieVotes: UILabel!
    @IBOutlet weak var imgMoviePopularity: UIImageView!
    @IBOutlet weak var imgMovieVotes: UIImageView!
    @IBOutlet weak var viewItemParent: UIView!
    

    override class func awakeFromNib() {
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 0))
    }

}
