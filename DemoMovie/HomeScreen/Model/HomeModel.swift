//
//  HomeModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import Foundation
import ObjectMapper
import CoreData

class HomeModel : Mappable {
    var page : Int?
    var totalPages : Int?
    var totalResults : Int?
    var results: [ResultModel]?
    var status_code: Int?
    var status_message: String?
    var success: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        page <- map["page"]
        totalPages <- map["totalPages"]
        totalResults <- map["totalResults"]
        results <- map["results"]
        status_code <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
    }
    
}


class ResultModel: NSObject,Mappable{
    var adult: Bool?
    var backdrop_path: String?
    var genre_ids: [Int]?
    var id: Int?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var poster_path: String?
    var release_date: String?
    var title: String?
    var video: Bool?
    var vote_average: Float?
    var vote_count: Int?
    var popularity: Float?
    var media_type: String?
    
    required init?(map: Map) {
        
    }
    
    init(original_title: String?,release_date: String?,vote_average: Float?,vote_count: Int?,popularity: Float?,poster_path: String?,id: Int?) {
        self.original_title = original_title
        self.release_date = release_date
        self.vote_average = vote_average
        self.vote_count = vote_count
        self.popularity = popularity
        self.poster_path = poster_path
        self.id = id
    }
    
    func mapping(map: Map) {
        
        adult <- map["adult"]
        backdrop_path <- map["backdrop_path"]
        genre_ids <- map["genre_ids"]
        id <- map["id"]
        original_language <- map["original_language"]
        original_title <- map["original_title"]
        overview <- map["overvasdiew"]
        poster_path <- map["poster_path"]
        release_date <- map["release_date"]
        title <- map["title"]
        video <- map["video"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]
        popularity <- map["popularity"]
        media_type <- map["media_type"]
    }
    
    
}
