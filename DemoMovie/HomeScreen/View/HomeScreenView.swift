//
//  HomeScreenView.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import UIKit
import SDWebImage
import CoreData



class HomeScreenView: AppMainVc {
    
    //MARK: - set outlets for the ui elements
    @IBOutlet weak var lblUserGreeting: UILabel!
    @IBOutlet weak var tblMovieList: UITableView!
    @IBOutlet weak var imgMoodBackground: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblNavBarHeading: UILabel!
    @IBOutlet weak var btnGenre: UIButton!
    @IBOutlet weak var imgGenre: UIImageView!
    
    lazy var homeViewModel: HomeViewModel = HomeViewModel()
    var homeModel: [ResultModel]? = [ResultModel](){
        didSet{
            DispatchQueue.main.async {
                self.tblMovieList.reloadData()
            }
        }
    }
    private var currentPage: Int = 1
    private var totalResultsPerPage = 20
    private var genreID: Int?
    private var genreName: String?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - set movie list tableview delegate and data source methods
        self.tblMovieList.delegate = self
        self.tblMovieList.dataSource = self
        
        self.setUISettings()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let genreID = self.genreID{
            self.lblNavBarHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: self.genreName ?? "")
            self.getMoviesByGenre(currentPage: self.currentPage, genreID: genreID)
        }else{
            self.lblNavBarHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: "Trending")
            self.getTrendingMovies(currentPage: self.currentPage)
        }
        
        NetworkManager.isUnreachable { NetworkManager in
            self.fetchRecords()
        }
        
    }
    
    
    //MARK: - set UI settings
    func setUISettings(){
        self.lblUserGreeting.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: greetUser().0)
        self.viewBackground.backgroundColor = UIColor.init(hexString: ColorCodes.cardShadowColor)
        self.lblNavBarHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: "Trending")
    }
    
    
    //MARK: - open genres view controller with animated effect
    @IBAction func openGenresVC(_ sender: UIButton){
        
        NetworkManager.isReachable { networkManagerInstance  in
            if let objGenresVC = Storyboard.main.instantiateViewController(withIdentifier: "GenresView") as? GenresView {
                self.homeModel?.removeAll()
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromRight
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                self.view.window!.layer.add(transition, forKey: kCATransition)
                objGenresVC.setSelectedGenreDelegate = self
                self.present(objGenresVC, animated: false, completion: nil)
            }
        }
        
        NetworkManager.isUnreachable { NetworkManager in
            super.showAleartViewWithTitle(ErrorMessages.noInternetTitle, message: ErrorMessages.noInternetMessage)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.currentPage = 1
        self.homeModel?.removeAll()
    }
    
}

//MARK: - tableview delegate and data source methods
extension HomeScreenView: UITableViewDelegate, UITableViewDataSource{
    
    
    //MARK: - tableview numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeModel?.count ?? 0
    }
    
    
    //MARK: - tableview cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.homeMovieItemCell, for: indexPath) as? HomeMovieItemCell{
            
            cell.selectionStyle = .none
            
            if let resultObj = self.homeModel?[indexPath.row]{
                //MARK: - set UI settings to the labels
                cell.lblMovieName.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 16, text: resultObj.original_title ?? "")
                cell.lblMovieReleaseYear.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: resultObj.release_date ?? "")
                cell.lblMoviePopularityValue.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 14, text: "\(resultObj.popularity ?? 0)")
                cell.lblMoviePopularity.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: "Popularity")
                cell.lblMovieVotes.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: "\(resultObj.vote_count ?? 0) Votes")
                cell.lblMovieVoteAvg.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 14, text: "\(resultObj.vote_average ?? 0)")
                
                NetworkManager.isReachable { networkManagerInstance  in
                    cell.imgMoviePoster.sd_imageIndicator = SDWebImageProgressIndicator.default
                    cell.imgMoviePoster.sd_setImage(with: URL(string: API.imagesBaseURL + (resultObj.poster_path ?? ""))) { uiImageObj, errorObj, cacheType, urlData in
                        if uiImageObj == nil{
                            if let decodedData = Data(base64Encoded: resultObj.poster_path ?? "", options: .ignoreUnknownCharacters) {
                                let image = UIImage(data: decodedData)
                                cell.imgMoviePoster.image = image
                            }
                        }
                    }
                }
                
                
                NetworkManager.isUnreachable { networkManagerInstance  in
                    if let decodedData = Data(base64Encoded: resultObj.poster_path ?? "", options: .ignoreUnknownCharacters) {
                        let image = UIImage(data: decodedData)
                        cell.imgMoviePoster.image = image
                    }
                }
                
                //MARK: - set corner radius to the image and parent view
                cell.viewItemParent.layer.cornerRadius = 8
                cell.imgMoviePoster.layer.cornerRadius = 4
            }
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK: - tableview heightForRowAt
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: - tableview willDisplay cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row == ((self.currentPage * self.totalResultsPerPage) - 1)){
            self.currentPage += 1
            if let genreID = self.genreID{
                self.getMoviesByGenre(currentPage: self.currentPage, genreID: genreID)
            }else{
                self.getTrendingMovies(currentPage: self.currentPage)
            }
        }
    }
    
    
    //MARK: - tableview set automatic row height for tableview
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //MARK: - navigate the user to the movie details screen if internet connection is available.
        
        NetworkManager.isReachable { networkManagerInstance  in
            if let resultObj = self.homeModel?[indexPath.row]{
                if let movieID = resultObj.id{
                    if let objMovieDetailVC = Storyboard.main.instantiateViewController(withIdentifier: "MovieDetailsView") as? MovieDetailsView {
                        objMovieDetailVC.movieID = movieID
                        self.navigationController?.pushViewController(objMovieDetailVC, animated: true)
                    }
                }
            }
        }
        
        
        NetworkManager.isUnreachable { NetworkManager in
            super.showAleartViewWithTitle(ErrorMessages.noInternetTitle, message: ErrorMessages.noInternetMessage)
        }
        
    }
    
}


//MARK: - set delegate to self to get the selected genre
extension HomeScreenView: SetSelectedGenreDelegate{
    func setSelectedGenre(selItem: Any) {
        if let selItem = selItem as? GenresModelData{
            
            if let genreID = selItem.id{
                self.genreID = genreID
                AppInstance.sharedInstance.selectedGenreID = genreID
                if let genreName = selItem.name{
                    self.genreName = genreName
                }
                //MARK:- check if the internet connection is available before making an API call
                NetworkManager.isReachable { networkManagerInstance  in
                    self.getMoviesByGenre(currentPage: self.currentPage, genreID: genreID)
                    self.lblNavBarHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.whiteColor, fontSize: 18, text: selItem.name ?? "")}
            }
        }
    }
}


//MARK: - API methods for parsing and success/failure methods
extension HomeScreenView: HomeViewModelDelegate{
    
    //MARK: - Make API Call for getting trending movies
    func getTrendingMovies(currentPage: Int){
        //MARK:- check if the internet connection is available before making an API call
        NetworkManager.isReachable { networkManagerInstance  in
            self.homeViewModel.getHomeDataFromServer(methodName: API.trendingMovies, delegate: self, bodyParam: "?page=\(currentPage)&api_key=\(API.APIKey)")
        }
    }
    
    
    
    //MARK: - success method for trending movies api if it success
    func didFetchHomeData(responseData: Any) {
        self.homeViewModel.parseHomeListData(completion: {isSuccess, message, title, dataObj in
            
            if(isSuccess){
                self.homeModel?.append(contentsOf: dataObj?.results ?? [ResultModel]())
                //MARK: - add the data to the core data to access offline
                    if let homeModel = self.homeModel{
                        for dataObj in homeModel{
                            self.createRecords(resultObj: dataObj)
                        }
                    }
                
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    //MARK: - failure method for trending movies api if it fails
    func didFailFetchHomeData(error: NSError) {
        
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
    
    
    
    //MARK: - Make API Call for getting movies by genres
    func getMoviesByGenre(currentPage: Int,genreID: Int){
        //MARK:- check if the internet connection is available before making an API call
        NetworkManager.isReachable { networkManagerInstance  in
            self.homeViewModel.getHomeDataFromServer(methodName: API.getMoviesByGenre,delegate: self,bodyParam:"?page=\(currentPage)&api_key=\(API.APIKey)&with_genres=\(genreID)")
        }
        
        NetworkManager.isUnreachable { NetworkManager in
            super.showAleartViewWithTitle(ErrorMessages.noInternetTitle, message: ErrorMessages.noInternetMessage)
        }
    }
    
    
    //MARK: - success method for genre api if it succeeds
    func didFetchHomeByGenreData(responseData: Any) {
        self.homeViewModel.parseHomeListData(completion: {isSuccess, message, title, dataObj in
            
            if(isSuccess){
                self.homeModel?.append(contentsOf: dataObj?.results ?? [ResultModel]())
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    //MARK: - failure method for genre api if it fails
    func didFailFetchHomeByGenreData(error: NSError) {
        
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
}


//MARK: -  Core Data methods for offline data persistence

//CoreData Source: https://medium.com/@ankurvekariya/core-data-crud-with-swift-4-2-for-beginners-40efe4e7d1cc
extension HomeScreenView{
    
    //MARK: - get image as data return the data as String to save it in the core data.
    func saveDataToDirectory(movieName: String, imageURL: String, completion: @escaping ((Data,Bool) -> Void)){
        // Create URL
        guard let url = URL(string: imageURL) else { return }
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            completion(data,true)
        }
        completion(Data(),false)
    }
    
    
    //MARK: - get image from URL
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    //MARK: - add records to core data
    func createRecords(resultObj: ResultModel){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let movieEntity = NSEntityDescription.entity(forEntityName: CoreDataKeys.entityName, in: managedContext)!
        
        let movie = NSManagedObject(entity: movieEntity, insertInto: managedContext)
        
        
        //MARK:- first we download the image as a form of data from the url and then convert this data obj to base64EncodedString and then save this string to the core data.
        self.saveDataToDirectory(movieName: resultObj.original_title ?? "", imageURL: API.imagesBaseURL + (resultObj.poster_path ?? "")) { dataObj,isSuccess  in
            if(isSuccess){
                movie.setValue(resultObj.id ?? 0, forKey: CoreDataKeys.movieID)
                movie.setValue(dataObj.base64EncodedString(), forKey: CoreDataKeys.movieImagePath)
                movie.setValue(resultObj.original_title ?? "", forKey: CoreDataKeys.movieName)
                movie.setValue(resultObj.release_date ?? "", forKey:  CoreDataKeys.movieReleaseYear)
                movie.setValue(resultObj.popularity ?? 0.0, forKey: CoreDataKeys.moviePopularity)
                movie.setValue(resultObj.vote_average ?? 0.0, forKey: CoreDataKeys.movieVoteAvg)
                movie.setValue(resultObj.vote_count ?? 0, forKey: CoreDataKeys.movieVoteCount)
            }
            
            do{
                try managedContext.save()
            }catch let error as NSError{
                print("Error occurred while saving data offline: \(error)")
            }
        }
        
    }
    
    
    //MARK: - get records from core data
    func fetchRecords(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataKeys.entityName)
        
        do{
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject]{
                let movieObj = ResultModel.init(original_title: data.value(forKey: CoreDataKeys.movieName) as? String, release_date: data.value(forKey: CoreDataKeys.movieReleaseYear) as? String, vote_average: data.value(forKey: CoreDataKeys.movieVoteAvg) as? Float, vote_count: data.value(forKey: CoreDataKeys.movieVoteCount) as? Int, popularity: (data.value(forKey: CoreDataKeys.moviePopularity) as? Float), poster_path: data.value(forKey: CoreDataKeys.movieImagePath) as? String, id: data.value(forKey: CoreDataKeys.movieID) as? Int)
                if let _ = data.value(forKey: CoreDataKeys.movieName) as? String{
                    self.homeModel?.append(movieObj)
                }
                
            }
        }catch let error as NSError{
            print("Error occurred while fetching offline data : \(error)")
        }
    }
    
    
    
}
