//
//  HomeViewModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  HomeViewModel delegate to get success/failure from API

protocol HomeViewModelDelegate {
    func didFetchHomeData(responseData: Any)
    func didFailFetchHomeData(error: NSError)
    
    func didFetchHomeByGenreData(responseData: Any)
    func didFailFetchHomeByGenreData(error: NSError)
}

class HomeViewModel:Networking,MainServiceDelegate{
    
    
    var homeViewModelDelegate: HomeViewModelDelegate?
    var currentAPI = ""
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
        if(currentAPI == API.trendingMovies){
            self.homeViewModelDelegate?.didFetchHomeData(responseData: responseData)
        }
        else if(currentAPI == API.getMoviesByGenre){
            self.homeViewModelDelegate?.didFetchHomeByGenreData(responseData: responseData)
        }
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
        
        if(currentAPI == API.trendingMovies){
            self.homeViewModelDelegate?.didFailFetchHomeData(error: error)
        }
        else if(currentAPI == API.getMoviesByGenre){
            self.homeViewModelDelegate?.didFailFetchHomeByGenreData(error: error)
        }
    }
    
    
    //MARK:-  Service call for getting trending movies for Home data
    func getHomeDataFromServer(methodName: String , delegate:HomeViewModelDelegate,bodyParam: String) -> Void {
        self.currentAPI = methodName
        self.homeViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.baseURL + methodName + bodyParam
        super.getCallWithAlamofire(serverUrl: url)
    }
    
    //MARK:-  Parsing Home data from JSON
    func parseHomeListData(completion: @escaping ((Bool , String , String , HomeModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<HomeModel>().map(JSONObject: responseDict)
            if(jsonData?.success != 0){
                completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false , jsonData?.status_message ?? "", ErrorMessages.APIAlertTitle,nil)
            }
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
    
    
}
