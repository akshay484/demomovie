//
//  MovieCastCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit
import SDWebImage

class MovieCastCell: UITableViewCell {
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var colCast: UICollectionView!
    private let spacing:CGFloat = 16.0
    
    var movieCastData: [CastModel]? = [CastModel](){
        didSet{
            DispatchQueue.main.async {
                self.colCast.reloadData()
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.colCast.delegate = self
        self.colCast.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}


//MARK: - UICollectionView Delegate and Datasource methods
extension MovieCastCell: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieCastData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCastColCell", for: indexPath) as? MovieCastColCell{
            
            
            if let resultObj = self.movieCastData?[indexPath.row]{
                
                if let profileImagePath = resultObj.profile_path{
                    cell.imgCastProfile.sd_imageIndicator = SDWebImageProgressIndicator.default
                    cell.imgCastProfile.sd_setImage(with: URL(string: API.imagesBaseURL + profileImagePath), completed: nil)
                }else{
                    cell.imgCastProfile.image = UIImage.init(named: "usericon")
                }
                
                cell.lblCastName.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 12, text: resultObj.original_name ?? "")
                cell.lblCastCharacterName.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textSecondaryColor, fontSize: 11, text: resultObj.character ?? "")
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    //MARK: - collectionView collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (1 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: 170)
    }
    
    
    
}
