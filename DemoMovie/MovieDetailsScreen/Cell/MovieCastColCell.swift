//
//  MovieCastColCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit

class MovieCastColCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgCastProfile: UIImageView!
    @IBOutlet weak var lblCastCharacterName: UILabel!
    @IBOutlet weak var lblCastName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func layoutSubviews() {
        self.imgCastProfile.setRoundImage()
    }

}
