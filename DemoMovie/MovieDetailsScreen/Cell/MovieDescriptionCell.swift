//
//  MovieDescriptionCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit

class MovieDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAbout: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
