//
//  MovieDetailsInfoCell.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit

class MovieDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var imgBackdrop: UIImageView!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var viewInfoParent: UIView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblMovieReleaseYear: UILabel!
    @IBOutlet weak var lblMovieDuration: UILabel!
    @IBOutlet weak var lblMovieGenres: UILabel!
    @IBOutlet weak var lblMoviePopularity: UILabel!
    @IBOutlet weak var lblMoviePopularityValue: UILabel!
    @IBOutlet weak var lblMovieVoteAvg: UILabel!
    @IBOutlet weak var lblMovieVotes: UILabel!
    @IBOutlet weak var imgMoviePopularity: UIImageView!
    @IBOutlet weak var imgMovieVotes: UIImageView!
    @IBOutlet weak var viewImg: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
