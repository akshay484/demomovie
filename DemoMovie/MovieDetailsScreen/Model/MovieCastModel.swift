//
//  MovieCastModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import Foundation
import ObjectMapper



class MovieCastModel : Mappable {
    
    var id : Int?
    var cast : [CastModel]?
    var status_code: Int?
    var status_message: String?
    var success: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cast <- map["cast"]
        id <- map["id"]
        status_code <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
    }
    
}

class CastModel : Mappable {
    
    var name : String?
    var original_name : String?
    var character : String?
    var profile_path : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        original_name <- map["original_name"]
        character <- map["character"]
        profile_path <- map["profile_path"]
    }
    
}
