//
//  MovieDetailModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import Foundation
import ObjectMapper


class MovieDetailModel : Mappable {
    var adult : Bool?
    var backdrop_path : String?
    var original_title : String?
    var overview : String?
    var genres: [GenresModelData]?
    var popularity: Float?
    var poster_path : String?
    var release_date : String?
    var runtime : Int?
    var vote_average : Int?
    var vote_count : Int?
    var id: Int?
    var status_code: Int?
    var status_message: String?
    var success: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        adult <- map["adult"]
        backdrop_path <- map["backdrop_path"]
        original_title <- map["original_title"]
        overview <- map["overview"]
        genres <- map["genres"]
        popularity <- map["popularity"]
        poster_path <- map["poster_path"]
        release_date <- map["release_date"]
        runtime <- map["runtime"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]
        id <- map["id"]
        status_code <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
    }
    
}
