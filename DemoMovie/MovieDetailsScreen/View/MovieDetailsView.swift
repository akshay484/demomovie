//
//  MovieDetailsView.swift
//  DemoMovie
//ss
//  Created by Aditya_mac_4 on 04/09/21.
//

import UIKit
import SDWebImage

class MovieDetailsView: AppMainVc {
    
    //MARK: - set outlets for the ui elements
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viewNavBar: UIView!
    lazy var movieDetailsViewModel: MovieDetailsViewModel = MovieDetailsViewModel()
    
    var movieID: Int = 0
    
    var currentAPI = ""
    
    var movieDetailModel: MovieDetailModel?{
        didSet{
            DispatchQueue.main.async {
                self.tblMain.reloadData()
            }
            if let movieID = self.movieDetailModel?.id {
                self.getMovieCastData(movieID: movieID)
            }
        }
    }
    
    var movieCastData: MovieCastModel?{
        didSet{
            DispatchQueue.main.async {
                self.tblMain.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .fade)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - set tblMain delegate and data source methods
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
        //MARK:- set UI Settings
        self.setUISettings()
        
        self.getMovieDetailsData(movieID: self.movieID)
    }
    
    //MARK: - set UI settings
    func setUISettings(){
        self.viewNavBar.backgroundColor = UIColor.init(hexString: ColorCodes.orangeColor)
    }
    
    //MARK: - pop self on click of back button
    @IBAction func dismissSelf(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK: - UITableView delegate and data source methods
extension MovieDetailsView:  UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.movieDetailsInfoCell, for: indexPath) as? MovieDetailsInfoCell{
                
                if let resultObj = self.movieDetailModel{
                    
                    //MARK: - set UI settings to the labels
                    cell.lblMovieName.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 15, text: resultObj.original_title ?? "")
                    cell.lblMovieName.sizeToFit()
                    cell.lblMovieReleaseYear.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: resultObj.release_date ?? "")
                    cell.lblMoviePopularityValue.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 12, text: "\(resultObj.popularity ?? 0)")
                    cell.lblMoviePopularity.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: "Popularity")
                    cell.lblMovieVotes.setTextStyles(font: Fonts.montserratRegular, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: "\(resultObj.vote_count ?? 0) Votes")
                    cell.lblMovieVoteAvg.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 12, text: "\(resultObj.vote_average ?? 0)")
                    cell.lblMovieDuration.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 12, text: "\("\(minutesToHoursAndMinutes(resultObj.runtime ?? 0).hours) h") \(minutesToHoursAndMinutes(resultObj.runtime ?? 0).leftMinutes) m")
                    cell.lblMovieGenres.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: resultObj.genres?.map({ resultObj in
                        return resultObj.name ?? ""
                    }).joined(separator: ", ") ?? "")
                    
                    cell.viewImg.backgroundColor = UIColor.init(hexString: ColorCodes.cardShadowColor)
                    
                    
                    cell.imgPoster.sd_imageIndicator = SDWebImageProgressIndicator.default
                    cell.imgPoster.sd_setImage(with: URL(string: API.imagesBaseURL + (resultObj.poster_path ?? "")), completed: nil)
                    
                    cell.imgBackdrop.sd_imageIndicator = SDWebImageProgressIndicator.default
                    cell.imgBackdrop.sd_setImage(with: URL(string: API.imagesBaseURL + (resultObj.backdrop_path ?? "")), completed: nil)
                    
                    
                    //MARK: - set corner radius to the image,add review button and parent view
                    cell.viewInfoParent.layer.cornerRadius = 8
                    cell.imgPoster.layer.cornerRadius = 4
                }
                
                return cell
            }
        }else if(indexPath.row == 1){
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.movieDescriptionCell, for: indexPath) as? MovieDescriptionCell{
                
                if let resultObj = self.movieDetailModel{
                    
                    cell.lblDescription.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textSecondaryColor, fontSize: 12, text: "\(resultObj.overview ?? "")")
                    
                    
                    cell.lblAbout.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 16, text: "About")
                }
                
                
                return cell
            }
        }else if(indexPath.row == 2){
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.movieCastCell, for: indexPath) as? MovieCastCell{
                
                if let resultObj = self.movieCastData{
                cell.lblHeading.setTextStyles(font: Fonts.montserratMedium, fontColor: ColorCodes.textPrimaryColor, fontSize: 16, text: "Cast")
                
                cell.movieCastData = resultObj.cast
                }
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


//MARK: - API Calls and Methods for getting movie details
extension MovieDetailsView: MovieDetailsViewModelDelegate{
    
    
    //MARK: - get movie details API call
    func getMovieDetailsData(movieID: Int){
        self.movieDetailsViewModel.getMovieDetailDataFromServer(methodName: API.getMovieDetail, delegate: self,bodyParam: "\(movieID)?api_key=\(API.APIKey)&language=en-US", callFor: ConstantValues.movieDetails)
    }
    
    //MARK: - get movie cast API call
    func getMovieCastData(movieID: Int){
        self.movieDetailsViewModel.getMovieDetailDataFromServer(methodName: API.getMovieDetail, delegate: self,bodyParam: "\(movieID)/credits?language=en-US&api_key=\(API.APIKey)", callFor: ConstantValues.movieCast)
    }
    
    
    
    //MARK: - success method for movie details api
    func didFetchMovieDetailData(responseData: Any) {
        self.movieDetailsViewModel.parseMovieDetailData(completion: { isSuccess, message, title, resultObj in
            if(isSuccess){
                self.movieDetailModel = resultObj
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    //MARK: - failure method for movie details api
    func didFailFetchMovieDetailData(error: NSError) {
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
    
    //MARK: - success method for movie cast api
    func didFetchMovieCastData(responseData: Any) {
        
        self.movieDetailsViewModel.parseMovieCastData(completion: { isSuccess, message, title, resultObj in
            if(isSuccess){
                self.movieCastData = resultObj
            }else{
                super.showAleartViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    //MARK: - failure method for movie cast api
    func didFailFetchMovieCastData(error: NSError) {
        super.showAleartViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
}
