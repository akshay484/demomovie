//
//  MovieDetailsViewModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  MovieDetailsViewModel delegate to get success/failure from API

protocol MovieDetailsViewModelDelegate: AnyObject {
    func didFetchMovieDetailData(responseData: Any)
    func didFailFetchMovieDetailData(error: NSError)
    
    func didFetchMovieCastData(responseData: Any)
    func didFailFetchMovieCastData(error: NSError)
}

class MovieDetailsViewModel:Networking,MainServiceDelegate{
    
    
    weak var movieDetailsViewModelDelegate: MovieDetailsViewModelDelegate?
    var currentAPI = ""
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
        if(currentAPI == ConstantValues.movieDetails){
            self.movieDetailsViewModelDelegate?.didFetchMovieDetailData(responseData: responseData)
        }
        if(currentAPI == ConstantValues.movieCast){
            self.movieDetailsViewModelDelegate?.didFetchMovieCastData(responseData: responseData)
        }
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
        if(currentAPI == ConstantValues.movieDetails){
            self.movieDetailsViewModelDelegate?.didFailFetchMovieDetailData(error: error)
        }
        if(currentAPI == ConstantValues.movieCast){
            self.movieDetailsViewModelDelegate?.didFailFetchMovieCastData(error: error)
        }
    }
    
    
    //MARK:-  Service call for movie detail data
    
    func getMovieDetailDataFromServer(methodName: String , delegate:MovieDetailsViewModelDelegate,bodyParam: String,callFor: String) -> Void {
        self.currentAPI = callFor
        self.movieDetailsViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.baseURL + methodName + bodyParam
        super.getCallWithAlamofire(serverUrl: url)
    }
    
    
    //MARK: - parse movie detail data
    func parseMovieDetailData(completion: @escaping ((Bool , String , String , MovieDetailModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<MovieDetailModel>().map(JSONObject: responseDict)
            
            if(jsonData?.success != 0){
            completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false , jsonData?.status_message ?? "", ErrorMessages.APIAlertTitle,nil)
            }
            
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
    //MARK: - parse movie cast data
    func parseMovieCastData(completion: @escaping ((Bool , String , String , MovieCastModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<MovieCastModel>().map(JSONObject: responseDict)
            
            if(jsonData?.success != 0){
            completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false , jsonData?.status_message ?? "", ErrorMessages.APIAlertTitle,nil)
            }
            
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
}

