//
//  SplashScreenView.swift
//  DemoMovie
//
//  Created by Aditya_mac_4_mac_4 on 30/08/21.
//

import Foundation
import UIKit

class SplashScreenView: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - navigate the user to the home screen from splash screen
        if let objHomeVC = Storyboard.main.instantiateViewController(withIdentifier: "HomeScreenView") as? HomeScreenView {
            self.navigationController?.pushViewController(objHomeVC, animated: true)
        }
    }
    
    
    
}
