//
//  Constants.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire

//MARK: -  Storyboards
struct Storyboard {
    static var launchScreen: UIStoryboard {
        return UIStoryboard(name: "LaunchScreen", bundle: Bundle.main)
    }
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}

//MARK: - API Endpoints, Base URL & API Keys
struct API{
    static let baseURL = "https://api.themoviedb.org/3/"
    static let APIKey = "49c1f77a8df2f60edf23b7bd94421754"
    static let trendingMovies = "trending/movie/day"
    static let getGenreList = "genre/movie/list"
    static let getMoviesByGenre = "discover/movie"
    static let imagesBaseURL = "https://image.tmdb.org/t/p/w500/"
    static let getMovieDetail = "movie/"
}

//MARK: - constant values
struct ConstantValues{
    static let movieDetails = "moviedetails"
    static let movieCast = "moviecast"
}

//MARK: - error message values
struct ErrorMessages{
    static let APIError = "Something went bad!"
    static let APIAlertTitle = "Alert"
    static let noInternetTitle = "No Internet Connection"
    static let noInternetMessage = "Please check your internet connection."
}


//MARK: - color code values
struct ColorCodes{
    static let textPrimaryColor =  "#808690"
    static let textSecondaryColor =  "#C4C6C8"
    static let cardBackgroundColor =  "#ffffff"
    static let cardShadowColor =  "#e9ebef"
    static let blackColor =  "#000000"
    static let whiteColor =  "#ffffff"
    static let orangeColor = "#ff8e5d"
    static let greenColor = "#59ca6d"
}


//MARK: - font values
struct Fonts{
    static let openSansSemibold =  "OpenSans-SemiBold"
    static let montserratMedium =  "Montserrat-Medium"
    static let montserratRegular =  "Montserrat-Regular"
    static let sfProDisplayRegular =  "SFProDisplay-Regular"
    static let openSansRegular =  "OpenSans-Regular"
    static let bariolBold =  "Bariol-Bold"
}


//MARK: - corner radius values
struct CornerRadius{
    static let standard = 8
}


//MARK: - keys of core data for offline data persistence
struct CoreDataKeys{
    static let entityName = "Movies"
    static let movieImagePath = "movieImagePath"
    static let movieName = "movieName"
    static let moviePopularity = "moviePopularity"
    static let movieReleaseYear = "movieReleaseYear"
    static let movieVoteAvg = "movieVoteAvg"
    static let movieVoteCount = "movieVoteCount"
    static let movieID = "movieID"
}

//MARK:- cell identifiers for table and collection views
struct CellIdentifiers{
    static let movieDetailsInfoCell = "MovieDetailsInfoCell"
    static let homeMovieItemCell = "HomeMovieItemCell"
    static let movieCastColCell = "MovieCastColCell"
    static let genreCollectionViewCell = "GenreCollectionViewCell"
    static let movieDescriptionCell = "MovieDescriptionCell"
    static let movieCastCell = "MovieCastCell"
}


/* Create request header */
func getHeader() -> HTTPHeaders {
    var header = HTTPHeaders()
    header["Accept"] = "application/json"
    header["Content-Type"] = "application/json"
    return header
}

func greetUser() ->(String,String){
    
    let dateComponents = Calendar.current.dateComponents([.hour], from: Date())
    
    var greetingString = ""
    var tintColor = ""
    
    if let hour = dateComponents.hour {
      switch hour {
      case 0..<12:
        greetingString = "Good Morning"
        tintColor = "#FDD65F"
      case 12..<17:
        greetingString = "Good Afternoon"
        tintColor = "#F8AC27"
      default:
        greetingString = "Good Evening"
        tintColor = "#1B1C2C"
      }
   }
    
    return (greetingString,tintColor)
}

class AppInstance: NSObject{
    
    static let sharedInstance: AppInstance = { return AppInstance() }()
    
    var selectedGenreID: Int?
}

func minutesToHoursAndMinutes (_ minutes : Int) -> (hours : Int , leftMinutes : Int) {
    return (minutes / 60, (minutes % 60))
}
