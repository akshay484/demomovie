//
//  UIButtonExtension.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 04/09/21.
//

import Foundation
import UIKit

extension UIButton{
    
    //MARK: - set text styles
    func setButtonStyles(font: String?,fontColor: String?,fontSize: CGFloat?,text: String,backgroundColor: String?){
        if let font = font,let fontColor = fontColor,let fontSize = fontSize, let backgroundColor = backgroundColor{
            self.titleLabel?.font = UIFont.init(name: font, size: fontSize)
            self.setTitleColor(UIColor.init(hexString: fontColor), for: .normal)
            self.setTitle(text, for: .normal)
            self.backgroundColor = UIColor.init(hexString: backgroundColor)
        }
    }
}

