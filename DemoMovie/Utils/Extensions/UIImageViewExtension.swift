//
//  UIImageViewExtension.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 06/09/21.
//

import Foundation
import UIKit

extension UIImageView {
    
    func setRoundImage() {
        DispatchQueue.main.async {
            super.layoutSubviews()
            let radius = self.frame.height/2.0
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = false
            self.clipsToBounds = true
            self.layoutIfNeeded()
        }
       
    }
}
