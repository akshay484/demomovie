//
//  UILabelExtension.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 31/08/21.
//

import Foundation
import UIKit

extension UILabel{
    
    //MARK: - set text styles
    func setTextStyles(font: String?,fontColor: String?,fontSize: CGFloat?,text: String){
        if let font = font,let fontColor = fontColor,let fontSize = fontSize{
        self.font = UIFont.init(name: font, size: fontSize)
        self.textColor = UIColor.init(hexString: fontColor)
        self.text = text
        self.font.withSize(fontSize)
        }
    }
}
