//
//  Networking.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


@objc protocol MainServiceDelegate : AnyObject {
    func didFetchData(responseData:Any) -> Void
    func didFailWithError(error:NSError) -> Void
}


class Networking{
    
    
    weak var mainServerdelegate:MainServiceDelegate?
    
    //--------------------------------------------------------------------------
    //MARK: - Get call through Alamofire ---------------------------------
    //--------------------------------------------------------------------------
    
    func getCallWithAlamofire(serverUrl: String) -> Void {
        
        print(serverUrl)
        
        let mainThread = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        mainThread.async {
            
            AF.request(serverUrl, method: .get, parameters: nil ,encoding: URLEncoding.default, headers:getHeader()).responseJSON {  response in
                
                switch response.result {
                case let .success(value):
                    self.mainServerdelegate?.didFetchData(responseData: value)
                    print("JSON: \(value)") // serialized json response
                    return
                case let .failure(error):
                    print(error.localizedDescription)
                    self.mainServerdelegate?.didFailWithError(error: error as NSError)
                    return
                }
            }
            
        }
    }
    
}
