//
//  Protocols.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import Foundation

protocol SetSelectedGenreDelegate: AnyObject{
    func setSelectedGenre(selItem: Any)
}
