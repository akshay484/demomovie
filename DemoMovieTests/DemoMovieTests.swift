//
//  DemoMovieTests.swift
//  DemoMovieTests
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import XCTest
@testable import DemoMovie

class DemoMovieTests: XCTestCase {
    var vcToTest: HomeScreenView!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vcToTest = Storyboard.main.instantiateViewController(withIdentifier: "HomeScreenView") as? HomeScreenView
        vcToTest.loadViewIfNeeded()
        vcToTest.viewDidLoad()
        vcToTest.viewWillAppear(true)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        vcToTest = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            vcToTest.getTrendingMovies(currentPage: 1)
        }
    }
    
    
    
    //MARK: - check if the home view has a tableview
    func testHasATableView() {
        XCTAssertNotNil(vcToTest.tblMovieList)
    }
    
    //MARK: - check if the home view tableview has a delegate set
    func testTableViewHasDelegate() {
        XCTAssertNotNil(vcToTest.tblMovieList.delegate)
    }
    
    //MARK: - check if the home view tableview conforms to the delegate protocol for methods didSelectRowAt
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(vcToTest.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.tableView(_:didSelectRowAt:))))
    }
    
    
    //MARK: - check if the home view tableview has a data source
    func testTableViewHasDataSource() {
        XCTAssertNotNil(vcToTest.tblMovieList.dataSource)
    }
    
    //MARK: - check if the home view tableview conforms to the datasource protocol for methods cellForRowAt, numberOfRowsInSection
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(vcToTest.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.tableView(_:cellForRowAt:))))
    }

    //MARK: - check if the home view tableview has correct number of rows at the beginning
    func testTableViewCellHasCorrectRows() {
        if(vcToTest.homeModel?.count ?? 0 > 0){
            XCTAssertEqual(vcToTest.tblMovieList.numberOfRows(inSection: 0) == 20, true)
        }
    }

}
