//
//  GenresTests.swift
//  DemoMovieTests
//
//  Created by Aditya_mac_4 on 06/09/21.
//

import XCTest
@testable import DemoMovie

class GenresTests: XCTestCase {
    var vcToTest: GenresView!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vcToTest = Storyboard.main.instantiateViewController(withIdentifier: "GenresView") as? GenresView
        vcToTest.loadViewIfNeeded()
        vcToTest.viewDidLoad()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        vcToTest = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    //MARK: - check if the home view has a Collectionview
    func testHasACollectionView() {
        XCTAssertNotNil(vcToTest.colGenreList)
    }
    
    //MARK: - check if the home view Collectionview has a delegate set
    func testCollectionViewHasDelegate() {
        XCTAssertNotNil(vcToTest.colGenreList.delegate)
    }
    
    //MARK: - check if the home view Collectionview conforms to the delegate protocol for methods didSelectRowAt
    func testCollectionViewConfromsToCollectionViewDelegateProtocol() {
        XCTAssertTrue(vcToTest.conforms(to: UICollectionViewDelegate.self))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.collectionView(_:didSelectItemAt:))))
    }
    
    
    //MARK: - check if the home view Collectionview has a data source
    func testCollectionViewHasDataSource() {
        XCTAssertNotNil(vcToTest.colGenreList.dataSource)
    }
    
    //MARK: - check if the home view Collectionview conforms to the datasource protocol for methods cellForRowAt, numberOfRowsInSection
    func testCollectionViewConformsToCollectionViewDataSourceProtocol() {
        XCTAssertTrue(vcToTest.conforms(to: UICollectionViewDataSource.self))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.collectionView(_:numberOfItemsInSection:))))
        XCTAssertTrue(vcToTest.responds(to: #selector(vcToTest.collectionView(_:cellForItemAt:))))
    }

}
